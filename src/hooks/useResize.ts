import { useCallback, useMemo, useRef, useState } from 'react';
import { throttle } from '../utils/trottle';
import { useEventListener } from './useEventListener';

export const THROTTLE_TIME = 100;

export const useResize = () => {
  const isResizing = useRef<boolean>(false);
  const containerRef = useRef<HTMLDivElement>(null);
  const leftPanelRef = useRef<HTMLDivElement>(null);
  const rightPanelRef = useRef<HTMLDivElement>(null);
  const handleRef = useRef<HTMLDivElement>(null);

  const [rightPanelSizes, setRightPanelSizes] = useState(() => ({
    width: window.innerWidth / 2,
    height: window.innerHeight,
  }));

  const throttleResize = useMemo(
    () =>
      throttle(() => {
        if (rightPanelRef.current) {
          const { width, height } = rightPanelRef.current.getBoundingClientRect();
          setRightPanelSizes({ width, height });
        }
      }, THROTTLE_TIME),
    []
  );

  const onMouseDown = useCallback(() => {
    isResizing.current = true;
  }, []);

  const onMouseUp = useCallback(() => {
    isResizing.current = false;
  }, []);

  const onResize = useCallback(() => {
    throttleResize();
  }, [throttleResize]);

  const onMouseMove = useCallback(
    (event: MouseEvent | Event) => {
      if (!isResizing.current) {
        return;
      }
      if (containerRef.current && leftPanelRef.current && rightPanelRef.current) {
        const offsetRight =
          containerRef.current.clientWidth - ((event as MouseEvent).clientX - containerRef.current.offsetLeft);
        leftPanelRef.current.style.right = offsetRight + 'px';
        rightPanelRef.current.style.width = offsetRight + 'px';
        throttleResize();
      }
    },
    [throttleResize]
  );

  useEventListener('mousedown', onMouseDown, handleRef.current);
  useEventListener('mousemove', onMouseMove, document);
  useEventListener('mouseup', onMouseUp, document);
  useEventListener('resize', onResize);

  return {
    containerRef,
    leftPanelRef,
    rightPanelRef,
    handleRef,
    rightPanelSizes,
  };
};
