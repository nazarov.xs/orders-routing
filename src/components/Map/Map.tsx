import { MapContainer, TileLayer } from 'react-leaflet';
import { FC, useEffect } from 'react';

import { selectSelectedOrder } from '../../store/orders/orders';
import { useAppDispatch, useAppSelector } from '../../store';
import { DEFAULT_CENTER, DEFAULT_ZOOM, HEIGHT_PADDING, MAP_URL } from './settings';
import { RouteLine } from './RouteLine';
import { useMapUpdate } from '../../hooks/useMapUpdate';
import { FETCH_ROUTING_ACTION } from '../../store/orders/sagas';

type Props = {
  sizes: { width: number; height: number };
};

export const Map: FC<Props> = ({ sizes }) => {
  const dispatch = useAppDispatch();
  const routingPoints = useAppSelector((state) => state.orders.points);
  const selectedOrder = useAppSelector(selectSelectedOrder);
  const mapRef = useMapUpdate(sizes, routingPoints ? routingPoints[0][0] : DEFAULT_CENTER);

  useEffect(() => {
    if (selectedOrder) {
      dispatch({ type: FETCH_ROUTING_ACTION, payload: selectedOrder });
    }
  }, [dispatch, selectedOrder]);

  return (
    <div style={{ height: sizes.height - HEIGHT_PADDING }}>
      <MapContainer className="map" center={DEFAULT_CENTER} zoom={DEFAULT_ZOOM} ref={mapRef}>
        <TileLayer url={MAP_URL} />
        <RouteLine points={routingPoints} />
      </MapContainer>
    </div>
  );
};
