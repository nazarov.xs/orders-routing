import { OrdersTable } from '../OrdersTable/OrdersTable';
import { Map } from '../Map/Map';
import { useResize } from '../../hooks/useResize';

export const ResizablePanel = () => {
  const { containerRef, leftPanelRef, rightPanelRef, handleRef, rightPanelSizes } = useResize();

  return (
    <div className="container" ref={containerRef}>
      <div className="left_panel" ref={leftPanelRef}>
        <OrdersTable />
      </div>
      <div className="right_panel" ref={rightPanelRef}>
        <div className="drag" ref={handleRef}></div>
        <Map sizes={rightPanelSizes} />
      </div>
    </div>
  );
};
