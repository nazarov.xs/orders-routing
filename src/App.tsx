import React from 'react';
import { ResizablePanel } from './components/ResizablePanel/ResizablePanel';

function App() {
  return (
    <div className="App">
      <ResizablePanel />
    </div>
  );
}

export default App;
