export const throttle = (callback: () => void, delay: number = 0) => {
  let inProgress = false;
  return () => {
    if (inProgress) {
      return;
    }
    inProgress = true;
    setTimeout(() => {
      callback();
      inProgress = false;
    }, delay);
  };
};
